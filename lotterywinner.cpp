#include <iostream>
int getUserInput();
bool linearSearch(int, const int[], const int);

int main() {
  const int size = 10;
  const int winningNumbers[size] = {13579, 26791, 26792, 33445, 55555, 62483, 77777, 79422, 85647, 93121};

  if(linearSearch(getUserInput(), winningNumbers, size)) {
    std::cout << "Found a winning number!" << std::endl;
  }
  else {
      std::cout << "Sorry didn't find a winning number :(" << std::endl;
  }

return 0;
}

bool linearSearch(int userInput, const int winningNumbers[], const int size) {
  int i = 0;
  do {
    if(userInput == winningNumbers[i]){
      return true;
    }
    else {
      i++;
    }
  } while(i < size);
  return false;
}

int getUserInput() {
  int userInput = 0;
  std::cout << "Please enter a 5 digit lottery number to see if it's a winning pair :";
  std::cin >> userInput;
  return userInput;
}
